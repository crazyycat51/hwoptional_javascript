"use strict"

let userNumber = prompt("Enter your number");
while (isNaN(userNumber) || !userNumber || userNumber >=1){
    userNumber = prompt("Enter your number correctly");
}

function factorial (n){
if (n == 1){
    return n;
}
else {
    return n * factorial(n-1);
}
}

alert(factorial(userNumber));

// 4! = 1*2*3*4 = 4*3*2 = n*(n-1)
// userNumber = n
// 4x(4(4-1))=48, 4!=24,  